<?php
require('db.connexion.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cuvillier Rémy | Compétences</title>
    <link rel="stylesheet" href="./css/styles.css" type="text/css" />

</head>

<body>
    <header>
        <nav>
            <div class="logo">
                <a href="/cv"><img src="./img/logo.png" alt="" /></a>
            </div>
            <div class="navbar">
                <ul>
                    <li>
                        <a href="/cv">Hello</a>
                    </li>
                    <li>
                        <a href="/cv-web/parcours">Parcours</a>
                    </li>
                    <li>
                        <a href="/cv/competences.php">Compétences</a>
                    </li>
                    <li>
                        <a href="/cv/projects.php">Projects</a>
                    </li>
                    <li>
                        <a href="/cv/contact.php">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <footer>
        <div class="link">

        </div>
    </footer>
</body>

</html>